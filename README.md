# Code for comparing shape systematics between R=0.4 and R=0.2 reclustered jets

This code was written to compare the shaped systematics derived in the MET-inclusive merged region from truth-level histos produced by [NtupleToHist code](https://gitlab.cern.ch/damacdon/monoswwtruthframework_ntupletohist/tree/master) for R=0.4 vs. R=0.2 reclustered jets.

## Usage

1. Copy the hist files that were produced on symmetry with the NtupleToHist code, with the [sub-jet radius set to either 02 or 04 in ReadTruthTree_MonoSWW.h](https://gitlab.cern.ch/damacdon/monoswwtruthframework_ntupletohist/blob/master/MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h#L242-245)

2. Run over the hist files to produce systematics using the instructions using the compareVariations.py script, as described [here](https://gitlab.cern.ch/damacdon/monoswwtruthframework_ntupletohist/tree/master#calculating-vjets-and-signal-systematics) in the NtupleToHist README. Change the `MonoSWWTruthFramework_NtupleToHist` name to `MonoSWWTruthFramework_NtupleToHist_TAR0804` for sub-jet radius 04 and `MonoSWWTruthFramework_NtupleToHist_TAR0802` for sub-jet radius 04, and put the two repos at the same directory level.

3. Put the `compareSysts.py` script contained in this repo at the same directory level as the `MonoSWWTruthFramework_NtupleToHist_TAR0802` and `MonoSWWTruthFramework_NtupleToHist_TAR0804` repos, and make a directory `Plots` at the same level to contain the plots created by running `compareSysts.py`.

4. Run `compareSysts.py` on the shape systematics root files in `MonoSWWTruthFramework_NtupleToHist_TAR0802` and `MonoSWWTruthFramework_NtupleToHist_TAR0804` as follows:

```bash
python compareSysts.py
```
