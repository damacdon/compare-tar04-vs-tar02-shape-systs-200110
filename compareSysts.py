from rootpy.io import root_open
from root_numpy import hist2array
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.gridspec import GridSpec
plt.rc('xtick', labelsize=20)
plt.rc('ytick', labelsize=20)

opts = ["TAR0802", "TAR0804"]
#types = ["2point", "pdf", "scale"]
types = ["2point"]
nLeps = [0, 1, 2]
categories = ["Merged"]
bkgs = ["Zjets", "Wjets"]

for type in types:
  for nLep in nLeps:
    for category in categories:
      for bkg in bkgs:
        gs = GridSpec(3, 1)  # 3 rows, 1 column
        fig = plt.figure(figsize =(9, 9))
        ax1=fig.add_subplot(gs[0:2,0])
        ax1.set_ylabel("Relative Shape Uncertainty", fontsize=20)
        ax1.get_xaxis().set_visible(False)
        ax2 = fig.add_subplot(gs[2, 0], sharex=ax1)
        #ax2.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))
        ax2.set_xlabel("Dark Higgs Candidate Mass [GeV]", fontsize=20)

        hists = []
        for opt in opts:
          path = "MonoSWWTruthFramework_%s/scripts/signal_variations/ShapeHists_%s/output%dLep_%s_%s_allptv_mS_candidate.root"%(opt, type, nLep, category, bkg)

          f = root_open(path)
          hist = f.sys
          hist_numpy, bin_edges = hist2array(hist, return_edges=True)
          bin_edges=bin_edges[0]
          bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
          hists.append(hist_numpy)
          ax1.plot(bin_centers, hist_numpy, drawstyle='steps-mid', label=opt)
        ax2.plot(bin_centers, hists[1]-hists[0], drawstyle='steps-mid', color="green")
        ax2.set_ylabel("Difference", fontsize=20)
        ax1.legend(prop={'size': 18}, ncol=2, loc='upper right')
        plt.tight_layout()
        plt.savefig("Plots/%dLep_%s_%s_allptv_%s_mS_candidate.png"%(nLep, category, bkg, type))
        plt.close()
